package ControllerGeneric;

import Annotation.MethodUrl;
import Exception.MethodNonSupporter;
import Exception.UrlNonSupporter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Utilitaire.ChargerMethode;
import Utilitaire.LoadClass;
import Utilitaire.ManipuleFichier;
import Utilitaire.Separateur;
import Utilitaire.SetterGeneraliser;
/**
 *
 * @author Mendrikaja
 */
public class FrontServlet extends HttpServlet {
    
    @Override
    public void init() {
        String project_dir_path = this.getInitParameter("project_path");
        ServletContext context = this.getServletContext();
        HashMap<String, Method> classpath_info = new HashMap<>();
        Method[][] all_methods = null;
            try {
                List<Class> all_classes = LoadClass.loadClasses(project_dir_path);
                all_methods = new Method[all_classes.size()][];
                for(int i = 0; i < all_classes.size(); i += 1) {
                    all_methods[i] = ChargerMethode.getClassAnnotatedMethods(all_classes.get(i));
                }
            } catch (ClassNotFoundException | IOException ex) {
                Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            for(int i = 0; i < all_methods.length; i += 1) {
                for(Method method: all_methods[i]) {
                    String methodUrl = method.getAnnotation(MethodUrl.class).url();

                    classpath_info.put(methodUrl, method);
                }
            }                
        context.setAttribute("classpath_info", classpath_info);
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, UrlNonSupporter, 
            IllegalAccessException, IllegalArgumentException, 
            InvocationTargetException, IOException, NoSuchMethodException, InstantiationException, MethodNonSupporter {
        ServletContext context = this.getServletContext();
            
        HashMap<String, Method> classpath_info = 
                (HashMap<String, Method>)context.getAttribute("classpath_info");
        
        String res = null;  
        try {
            res = Separateur.splitUrl(request.getRequestURL().toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        Method toInvokeMethod = null;
        
        if(classpath_info.containsKey(res)) {
            toInvokeMethod = classpath_info.get(res);
        } else throw new UrlNonSupporter("UrlNotSupportedException: cannot find method with url '"+res+"'");
        
        if(request.getParameterNames().hasMoreElements()) {
            Class currentClass = toInvokeMethod.getDeclaringClass();
            Field[] currentClassFields = currentClass.getDeclaredFields();
            Object currentObj = currentClass.getConstructor().newInstance();
                
            for(Field field: currentClassFields) {
                if(request.getParameter(field.getName()) != null) {
                    Method setter = ChargerMethode.findMethod(currentClass, "set"+field.getName());
                    SetterGeneraliser.set(currentObj, setter, request.getParameter(field.getName()), field);
                }
            }
            
//          // must review
            ModelView modelview = (ModelView) toInvokeMethod.invoke(currentObj);
            response.sendRedirect(modelview.getUrl());
        } else {
            ModelView modelview = (ModelView) toInvokeMethod.invoke(toInvokeMethod.getDeclaringClass().getConstructor().newInstance());
            Set<String> keys_sets = modelview.getData().keySet();
            List<String> keys = new ArrayList<>(keys_sets);
            keys.forEach((key) -> {
                request.setAttribute(key, modelview.getData().get(key));
            });
            RequestDispatcher reqDispatcher = request.getRequestDispatcher(modelview.getUrl());
            reqDispatcher.forward(request, response);
        }   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        try {
            processRequest(request, response);
        } catch (UrlNonSupporter | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | InstantiationException | MethodNonSupporter ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (UrlNonSupporter | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | InstantiationException | MethodNonSupporter ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
