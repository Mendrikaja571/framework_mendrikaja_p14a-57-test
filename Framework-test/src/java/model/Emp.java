/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Annotation.MethodUrl;
import ControllerGeneric.ModelView;
import java.sql.Date;
import java.util.HashMap;

/**
 *
 * @author Mendrikaja
 */
public class Emp {
    private String values;
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }
    
    @MethodUrl(url="Read")
    public ModelView fetchEmp() {
        ModelView mv = new ModelView();
        
        mv.setUrl("test.jsp");
        
        String[] dataAndrana = {"Salut","Ca va"};
        
        HashMap<String, Object> tdata = new HashMap<>();
        tdata.put("dataAndrana", dataAndrana);
        
        mv.setData(tdata);
        
        return mv;
    }
    
    public void ModelsMethUrl() {
        System.out.println("Ici");
    }

}
