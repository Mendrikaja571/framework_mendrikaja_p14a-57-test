package Utilitaire;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Mendrikaja
 */
public class ManipuleFichier {
    public static ArrayList<String> read(String file) throws FileNotFoundException, IOException {
        ArrayList<String> lines = new ArrayList<>();
        FileReader fr = new FileReader(new File(file));
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line = br.readLine()) != null) lines.add(line);
        
        return lines;
    }
    
    public static String readSingleLine(String file) throws FileNotFoundException, IOException {
        FileReader fr = new FileReader(new File(file));
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        
        return line;
    }
}
