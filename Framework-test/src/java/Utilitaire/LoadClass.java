package Utilitaire;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mendrikaja
 */
public class LoadClass {
    
    public static ArrayList<Class> loadClasses(String project_dir_path) throws ClassNotFoundException, IOException {
        String conf_file_path = project_dir_path+"\\class_packages.conf";
        String classesPath = project_dir_path+"\\build\\web\\WEB-INF\\classes\\";
        
        ArrayList<String> packages = ManipuleFichier.read(conf_file_path);
        ArrayList<Class> classes = new ArrayList<>();

        for(int p = 0; p < packages.size(); p += 1) {
            String pckgname = packages.get(p);
            String currClassPath = classesPath+pckgname;
            StringBuffer sb = new StringBuffer(currClassPath);
            File rep = new File(sb.toString());
            System.out.println("File: "+rep);
            
            if(rep.exists() && rep.isDirectory()){
                FilenameFilter filter = new DotClassFilter();
                File[] list = rep.listFiles(filter);
                
                for (File sub_list : list) {
                    classes.add(Class.forName(pckgname + "." + sub_list.getName().split("\\.")[0]));
                }
            }
        }
 
	return classes;
    }

    public static List<Class> getAnnotatedClasses(String models) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public LoadClass() {
    }

   
    
    private static class DotClassFilter implements FilenameFilter{
            @Override
            public boolean accept(File arg0, String arg1) {
                    return arg1.endsWith(".class");
            }
    }
}
