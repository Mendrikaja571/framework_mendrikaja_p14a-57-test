package Utilitaire;

import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 *
 * @author Mendrikaja
 */
public class Separateur {
    public static String splitUrl(String url) throws MalformedURLException {
        String[] preSplitedUrl = url.split("/");
        ArrayList<Character> splittedMethod = Separateur.splitByChar(preSplitedUrl[preSplitedUrl.length - 1]);
        Integer limit = splittedMethod.size() - 3;
        String finalUrl = buildString(splittedMethod, limit);
        
        return finalUrl;
    }
    
    private static ArrayList<Character> splitByChar(String str) {
        char[] character = str.toCharArray();
        ArrayList<Character> splitedWord = new ArrayList<>();
        for (int i = 0; i < character.length; i++) {
            splitedWord.add(Character.valueOf(character[i]));
        }
        return splitedWord;
    }
    
    private static String buildString(ArrayList<Character> splittedStr, Integer limit) {
        String str = "";
        for(int i = 0; i < limit; i += 1) {
            str += splittedStr.get(i);
        }
        return str;
    }
}
