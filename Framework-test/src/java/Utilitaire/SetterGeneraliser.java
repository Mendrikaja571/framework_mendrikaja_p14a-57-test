package Utilitaire;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 *
 * @author Mendrikaja
 */
public class SetterGeneraliser {
    public static void set(Object invoker, Method setter, String value, Field field) throws NoSuchMethodException, InstantiationException,IllegalAccessException, IllegalArgumentException,InvocationTargetException {
        String typeName = field.getType().getName();
        if(typeName.compareTo("int") == 0 || typeName.compareTo("java.lang.Integer") == 0) setter.invoke(invoker, Integer.parseInt(value));
        else if(typeName.compareTo("double") == 0 || typeName.compareTo("java.lang.Double") == 0) setter.invoke(invoker, Double.parseDouble(value));
        else if(typeName.compareTo("float") == 0 || typeName.compareTo("java.lang.Float") == 0) setter.invoke(invoker, Float.parseFloat(value));
        else if(typeName.compareTo("java.lang.String") == 0) setter.invoke(invoker, value);
        else if(typeName.compareTo("java.sql.Date") == 0) {
            LocalDate localDate = LocalDate.parse(value);
            Integer yr = localDate.getYear(), mth = localDate.getMonthValue(), day = localDate.getDayOfMonth();
            setter.invoke(invoker, new java.sql.Date(yr - 1901, mth + 11, day));
        } 
        else if(typeName.compareTo("java.util.Date") == 0) {
            LocalDate localDate = LocalDate.parse(value);
            Integer yr = localDate.getYear(), mth = localDate.getMonthValue(), day = localDate.getDayOfMonth();
            setter.invoke(invoker, new java.util.Date(yr - 1901, mth, day));
        }
        else if(typeName.compareTo("java.time.LocalDate") == 0) setter.invoke(invoker, LocalDate.parse(value));
        else if(typeName.compareTo("java.sql.Time") == 0) {
            LocalDateTime time = LocalDateTime.parse(value);
            Integer hr = time.getHour(), min = time.getMinute(), sec = time.getSecond();
            setter.invoke(invoker, new Time(hr, min, sec));
        }
        else if(typeName.compareTo("java.time.LocalDateTime") == 0) setter.invoke(invoker, LocalDateTime.parse(value));
    }
}
