package Utilitaire;

import Annotation.MethodUrl;
import Exception.MethodNonSupporter;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 *
 * @author Mendrikaja
 */
public class ChargerMethode {
    public static Method[] getClassAnnotatedMethods(Class declaringClass) {
        Method[] all_methods = declaringClass.getDeclaredMethods();
        ArrayList<Method> annotatedMethods = new ArrayList<>();
        
        for(Method method: all_methods) {
            if(method.isAnnotationPresent(MethodUrl.class)) annotatedMethods.add(method);
        }
        
        return annotatedMethods.toArray(all_methods);
    }
    
    public static Method findMethod(Class methodOwner, String toFindMethodName) throws MethodNonSupporter {
        Method[] ownerMethods = methodOwner.getDeclaredMethods();
        
        for(int i = 0; i < ownerMethods.length; i += 1) {
            if(ownerMethods[i].getName().compareToIgnoreCase(toFindMethodName) == 0) return ownerMethods[i];
        }
        throw new MethodNonSupporter("Method with name: "+toFindMethodName+" not found in the class "+methodOwner.getName());
    }
}
